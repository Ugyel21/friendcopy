//jshint esversion:6
const express = require("express");
const router = express.Router();
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const Booking = require("../models/bookingModel");
const {
  transporter
} = require("../config/email");

// SET STORAGE
var storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, 'routes/uploads');
  },
  filename: function(req, file, cb) {
    cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
  }
});

var upload = multer({
  storage: storage
});

router.get("/userPaymentForm", function(req, res) {
  res.render("userPaymentForm");
});

router.post("/userPaymentForm", upload.single("image"), function(req, res) {
  const obj = {
    img: {
      data: fs.readFileSync(path.join(__dirname + "/uploads/" + req.file.filename)),
      contentType: "image/png"
    }
  };
  console.log("PATHHHHH"+req.file.filename);
  Booking.findOne({
    email: req.body.email
  }, function(err, foundBooked) {
    if(foundBooked){
      if (foundBooked.payment === false) {
        if (foundBooked) {
          Booking.updateOne({
            _id: foundBooked._id
          }, {
            img: obj.img
          }, function(err) {
            if (err) {
              console.log(err);
            } else {
              var mailOptions = {
                from: req.body.email,
                to: process.env.auth_user,
                subject: "Gyelpozhing Turf Booking - Payment made",
                attachments: [{ // stream as an attachment
                  filename: req.file.filename,
                  path: path.join(__dirname + "/uploads/" + req.file.filename)
                }],
                html: '<h3> From: ' + req.body.email + '</h3>Hello Admin, i am ' + foundBooked.name + ', i have payed ground fee la.<h4><br>Thank you la.'
              };

              //sending mail
              transporter.sendMail(mailOptions, function(error, info) {
                if (error) {
                  res.render("userPaymentForm", {
                    errorMessage: error
                  });
                } else {
                  res.render("userPaymentForm", {
                    successMessage: "Your Payment has been successful. Please wait for the confirmation message."
                  });
                }
              });
            }
          });
        } else {
          res.render("userPaymentForm", {
            errorMessage: "The current email was not used for ground booking. Please try with correct booked email!"
          });
        }
      } else {
        res.render("userPaymentForm", {
          successMessage: "You have already paid your ground fee. Thank you for using our service."
        });
      }
    }else{
      res.render("userPaymentForm", {
        errorMessage: "The current email was not used for ground booking. Please try with correct booked email!"
      });
    }
  });
});

module.exports = router;
